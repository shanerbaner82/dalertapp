<?php

class Contacts extends Eloquent {

    protected $table = 'contacts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'number', 'user_email'];


}
