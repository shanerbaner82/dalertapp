<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::post('queue/sms', function(){
	return Queue::marshal();
});

Route::get('/', function()
{
	Queue::push('SMSController',['time' => time()]);


});
Route::resource('cars', 'CarsController');
Route::resource('contacts', 'ContactsController');
Route::resource('users', 'UsersController');
Route::get('push/{message}/{license}/{state}', 'PushController@push');
Route::get('sms/{license}/{state}/{message}/{lat}/{long}', 'SMSController@sendMessage');



Route::post('auth/register', 'AuthController@create');
Route::post('auth/regidupdate', 'AuthController@regidupdate');
Route::post('auth/login', 'AuthController@login');
