<?php



class SMSController extends \BaseController {

	public function sendMessage($license, $state, $message, $lat, $long)
	{
		//Get the Car
		$car = Car::where('state', '=', $state)->where('license', '=', $license)->get();

//		var_dump(json_encode($car));

		//Get the user
		$user_email = $car[0]['email'];
		$user = User::where('email', '=',$user_email)->get();
		$name = $user[0]['name'];

//		//Get all ICE
		$contactsArray = Contacts::where('user_email', '=', $user_email)->get();
//
//
        //Build the Map URL

        $link = "https://maps.googleapis.com/maps/api/staticmap?center=".$lat.",".$long."&zoom=15&size=1200x1200&markers=color:orange%7Clabel:%7C".$lat.",".$long;

        $data = array('longUrl' => $link, 'key' => "AIzaSyCa73oq-9uqJDY6pJi2aWWoJ_sWNrIICn4");
        $data = json_encode($data);

        $context = [
            'http' => [
                'method' => 'post',
                'header'=>'Content-Type:application/json',
                'content' => $data
            ]
        ];

        $context = stream_context_create($context);
        $result = file_get_contents('https://www.googleapis.com/urlshortener/v1/url', false, $context);

        $json = json_decode($result);
        $url = $json->id;




		$contactCollection = [];
		foreach($contactsArray as $contact){
			$number = $contact['number'];
			$rawNumber = str_replace('-', '', $number);
			if(substr($rawNumber, 0, 1) != '1'){
				$rawNumber = '1' . $rawNumber;
			}
			array_push($contactCollection, $rawNumber);
			Queue::push('SMSController', [
				'number' => $rawNumber,
				'message' => $name .' '. $message . ' ' .$url
			]);
		}
	}

	public function fire($job, $data){
//		File::append(app_path().'/time.txt', $data['message'] . PHP_EOL);
		$sms = NexmoSmsMessage::sendText($data['number'],'12132633355', $data['message']);
	}
}
