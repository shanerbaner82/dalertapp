<?php

class AuthController extends \BaseController {


	public function create()
	{
		$user = new User;
		$user->name = Input::get('name');
		$user->email = Input::get('email');
		$user->password = Hash::make(Input::get('name'));
		$user->gcmid = Input::get('gcmid');
		$user->save();
		return 'Done';
	}

	public function login()
	{
		if (Auth::attempt(Input::only('email', 'password')))
		{
			return Auth::user();
		}
		else{
			return 'FAILED';
		}
	}

    public function regidupdate(){

        $email = Input::get('email');
        $user = User::where('email', '=', $email)->first();
        $cars = Car::where('email', '=', $email)->get();
        $id = Input::get('regid');
        foreach ($cars as $car){
            $car->gcmid = $id;
            $car->save();
        }
        $user->gcmid =$id;
        $user->save();

        return $cars;
    }

}
